// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6 (swiftlang-5.6.0.323.62 clang-1316.0.20.8)
// swift-module-flags: -target i386-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name FccAbstractCore
@_exported import FccAbstractCore
import Foundation
import Swift
import UIKit
import _Concurrency
public class FontLoadRequest : FccAbstractCore.Parcel, Swift.Codable {
  @objc public func pack() -> Any
  public var fontAssetMap: [Swift.String : [Swift.UInt8]]
  public init(fontAssetMap: [Swift.String : [Swift.UInt8]])
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@objc public class FccData : ObjectiveC.NSObject, FccAbstractCore.AbstractFccData {
  public var nativeHandler: FccAbstractCore.NativeHandler
  public var controller: UIKit.UIViewController
  @objc public init(nativeHandler: FccAbstractCore.NativeHandler, controller: UIKit.UIViewController)
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class LocalFontConfig : ObjectiveC.NSObject {
  public var fontVariant: Swift.String
  public var fontFileResource: Foundation.Data?
  @objc convenience public init(fontVariant: Swift.String, fontFileResource: Foundation.Data)
  @objc override dynamic public init()
  @objc deinit
}
public protocol AbstractUIViewController {
  func displayUI(parcel: FccAbstractCore.Parcel)
  func buildUI(parcel: FccAbstractCore.Parcel)
  func createPrivateChannel(parcel: FccAbstractCore.Parcel)
  func hideUI()
  func showTransactionApprovedScreen()
  func showTransactionDeclinedScreen()
  func showProgressScreen()
  func hideProgressScreen()
  func loadCustomFont(parcel: FccAbstractCore.Parcel)
}
@_inheritsConvenienceInitializers @objc public class FlutterEngineDependency : ObjectiveC.NSObject {
  public var flutterEngineId: Swift.String
  public var secureChannelName: Swift.String
  @objc convenience public init(flutterEngineId: Swift.String, secureChannelName: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
public protocol Channel {
  associatedtype CT
  var channel: Self.CT { get }
  func flowThrough(receiver: FccAbstractCore.CrossPlatformUIReceivers, parcel: FccAbstractCore.Parcel?)
}
public protocol Messenger {
  associatedtype C
  func send(reciever: FccAbstractCore.CrossPlatformUIReceivers, parcel: FccAbstractCore.Parcel?, channel: Self.C)
}
public class PrivateChannelData : FccAbstractCore.Parcel, Swift.Codable {
  @objc public func pack() -> Any
  public var channelName: Swift.String
  public init(channelName: Swift.String)
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@objc public class DeclinedTransactionData : ObjectiveC.NSObject {
  @objc public var didUserForgetPin: Swift.Bool
  public init(didUserForgetPin: Swift.Bool)
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class TransactionRequest : ObjectiveC.NSObject, Swift.Codable, FccAbstractCore.Parcel {
  @objc override dynamic public init()
  @objc convenience public init(authType: Swift.Int, data: FccAbstractCore.AuthData)
  @objc public func pack() -> Any
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
public protocol NotificationHandler {
  func sendNotification(receiver: FccAbstractCore.CrossPlatformUIReceivers, parcel: FccAbstractCore.Parcel?)
  func registerNotificationReceiver(receiver: FccAbstractCore.NotificationReceiver)
}
@objc public protocol AbstractFlutterEngineDependency {
}
@_inheritsConvenienceInitializers @objc public class LocalStorageCustomFontConfig : ObjectiveC.NSObject, FccAbstractCore.CustomFontConfig {
  public var fontConfigList: [FccAbstractCore.LocalFontConfig]
  @objc convenience public init(fontConfigList: Swift.Array<FccAbstractCore.LocalFontConfig>)
  @objc override dynamic public init()
  @objc deinit
}
@objc public protocol CustomFontConfig {
}
@objc public protocol AbstractFccData {
}
@objc public protocol Parcel {
  @objc func pack() -> Any
}
public enum CrossPlatformUIReceivers : Swift.String {
  case DISPLAY_UI
  case HIDE_UI
  case BUILD_UI
  case SHOW_TRANSACTION_APPROVED_SCREEN
  case SHOW_TRANSACTION_DECLINED_SCREEN
  case CREATE_PRIVATE_CHANNEL
  case HIDE_PROGRESS_SCREEN
  case SHOW_PROGRESS_SCREEN
  case LOAD_CUSTOM_FONTS
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
@objc public protocol NativeHandler {
  @objc func waterMarkScreen(bitmap: [Swift.UInt8])
  @objc func approveTransaction(data: Any?)
  @objc func declineTransaction()
  @objc func onScreenReady()
  @objc func detectOverlay()
  @objc func onBackButtonPressed()
  @objc func invalidMethodCall(methodName: Swift.String)
  @objc func declineTransaction(data: FccAbstractCore.DeclinedTransactionData)
}
@_inheritsConvenienceInitializers @objc public class AuthData : ObjectiveC.NSObject, Swift.Codable {
  @objc override dynamic public init()
  @objc convenience public init(uiJsonProperties: Swift.String, useFallbackScreen: Swift.Bool)
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@objc public protocol FccApi {
  @objc func displayUI(parcel: FccAbstractCore.Parcel)
  @objc func buildUI(parcel: FccAbstractCore.Parcel)
  @objc func attachView(fccData: FccAbstractCore.FccData)
  @objc func prepare(flutterEngineDependency: FccAbstractCore.FlutterEngineDependency)
  @objc func prepare(flutterEngineDependency: FccAbstractCore.FlutterEngineDependency, customFontConfig: FccAbstractCore.CustomFontConfig)
  @objc func hideUI()
  @objc func showTransactionApprovedScreen()
  @objc func showTransactionDeclinedScreen()
  @objc func showProgressScreen()
  @objc func hideProgressScreen()
  @objc func isFccReady() -> Swift.Bool
}
public protocol NotificationReceiver {
  func handleNotification(channel: Any)
}
extension FccAbstractCore.CrossPlatformUIReceivers : Swift.Equatable {}
extension FccAbstractCore.CrossPlatformUIReceivers : Swift.Hashable {}
extension FccAbstractCore.CrossPlatformUIReceivers : Swift.RawRepresentable {}
