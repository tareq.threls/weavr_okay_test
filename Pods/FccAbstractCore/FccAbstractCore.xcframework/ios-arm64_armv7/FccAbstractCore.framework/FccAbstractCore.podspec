Pod::Spec.new do |spec|
  spec.name         = 'FccAbstractCore'
  spec.version      = '1.0.9'
  spec.license      = { :type => 'Apache 2.0', :file => 'LICENSE' }
  spec.homepage     = 'https://okaythis.com'
  spec.author       = { 'Okay This' => 'hello@okaythis.com' }
  spec.summary      = 'FccAbstractCore'
  spec.source       = { :git => "https://github.com/Okaythis/FccAbstractCoreSDK.git", :tag => "#{spec.version}" }
  spec.source_files  = "FccAbstractCore/**/*.{h,m,swift}"
  spec.requires_arc = true
  spec.ios.deployment_target = '10.0'
  spec.xcconfig = { 'SWIFT_INSTALL_OBJC_HEADER' => 'NO' }
  spec.xcconfig = { 'DEFINES_MODULE' => 'TRUE' }
  spec.swift_versions = '5.0'
end
