// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>
#import "OperationType.h"
#import "TransactionInfo.h"

@protocol ResourceProvider
@required

@property NSAttributedString *biometricAlertReasonText;
@property NSAttributedString *confirmButtonText;
@property NSAttributedString *confirmBiometricTouchButtonText;
@property NSAttributedString *confirmBiometricFaceButtonText;
@property NSAttributedString *cancelButtonText;
@property NSAttributedString *massPaymentDetailsButtonText;
@property NSAttributedString *massPaymentDetailsHeaderText;
@property NSAttributedString *feeLabelText;
@property NSAttributedString *recepientLabelText;
@property NSAttributedString *enrollmentTitleText;
@property NSAttributedString *enrollmentDescriptionText;

- (NSAttributedString*)stringForTransactionInfo:(TransactionInfo*)transactionInfo;
- (NSAttributedString*)stringForConfirmActionHeader:(TransactionInfo*)transactionInfo;

@end
