// Copyright © Protectoria. All rights reserved.

#import "OperationType.h"
#import "AuthMethod.h"
#if __has_include(<DefaultPsaUi/DefaultPsaUi.h>)
    #import <DefaultPsaUi/DefaultPsaUi.h>
#else
    #import <ePaymentsUI/ePaymentsUI.h>
#endif

@interface TransactionInfo : NSObject

@property OperationType operationType;
@property AuthMethod authMethod;
@property NSArray<PSAPayment *> *payments;
@property NSDictionary<NSString*, NSString*> *args;

@end
