// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>
#import <PSACommon/PSACommon.h>
#import <FccAbstractCore/FccAbstractCore-Swift.h>
#import "PSATenant.h"
#import "ResourceProvider.h"

@protocol FccApi;

NS_ASSUME_NONNULL_BEGIN
typedef void (^PSAUnlinkCompletionBlock)(PSASharedStatuses status, NSNumber *tenantId);
typedef void (^PSALinkCompletionBlock)(PSASharedStatuses status, PSATenant *tenant);

@interface PSA : NSObject

+ (void)reset;
+ (void)updateDeviceToken:(NSString *)deviceToken;
+ (BOOL)isReadyForEnrollment;
+ (BOOL)isEnrolled;
+ (void)startEnrollmentWithHost:(nullable NSString *)host
                      invisibly:(bool)invisibly
                 installationId:(nullable NSString *)installationId
               resourceProvider:(nullable id <ResourceProvider>)resourceProvider
                   pubPssBase64:(nullable NSString *)pubPssBase64
                   idCompletion:(void (^)(PSASharedStatuses))completion;
+ (BOOL)isReadyForAuthorization;

+ (void)update:(id<FccApi>)fccApiImpl;
+ (void)update:(id<FccApi>)fccApiImpl fontConfig:(LocalStorageCustomFontConfig *)config;

+ (void)startAuthorizationWithTheme:(nullable PSATheme *)theme
                          sessionId:(nullable NSNumber *)sessionId
                         completion:(void (^)(BOOL, PSASharedStatuses))completion;

+ (void)startAuthorizationWithTheme:(nullable PSATheme *)theme
                          sessionId:(nullable NSNumber *)sessionId
                   resourceProvider:(nullable id<ResourceProvider>)resourceProvider
               loaderViewController:(nullable UIViewController *)loaderViewController
                         completion:(void (^)(BOOL, PSASharedStatuses, TransactionInfo * _Nullable))completion;

+ (void)onApplicationWillResignActive:(UIApplication *)application;
+ (void)onApplicationWillEnterForeground:(UIApplication *)application;
+ (void)linkTenantWithLinkingCode:(NSString *)linkingCode
                       completion:(void (^)(PSASharedStatuses, PSATenant*))completion;
+ (void)unlinkTenantWithTenantId:(NSNumber *)tenantId
                      completion:(void (^)(PSASharedStatuses, NSNumber*))completion;
+ (void)updateTenantWithTenantId:(NSNumber *)tenantId
                      completion:(void (^)(PSASharedStatuses, PSATenant*))completion;

+ (void)sendDeviceLivelinessSettings:(void (^)(PSASharedStatuses))completion;

+ (nullable NSData *)decryptData:(NSData *)encryptedData;

@end
NS_ASSUME_NONNULL_END
