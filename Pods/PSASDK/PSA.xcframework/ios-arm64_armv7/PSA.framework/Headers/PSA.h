// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for PSA.
FOUNDATION_EXPORT double PSAVersionNumber;

//! Project version string for PSA.
FOUNDATION_EXPORT const unsigned char PSAVersionString[];

// In this header, you should import all the public headers of your framework using statements like
#import "PSAPublic.h"
#import "PSATenant.h"
#import "TransactionInfo.h"
#if __has_include(<DefaultPsaUi/DefaultPsaUi.h>)
    #import <DefaultPsaUi/PSASecurityProtocol.h>
#else
    #import <ePaymentsUI/PSASecurityProtocol.h>
#endif
