// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, AuthMethod){
    AuthMethodOK  = 1,
    AuthMethodPIN = 2,
    AuthMethodBIO = 3,
    AuthMethodSETPIN = 4
};
