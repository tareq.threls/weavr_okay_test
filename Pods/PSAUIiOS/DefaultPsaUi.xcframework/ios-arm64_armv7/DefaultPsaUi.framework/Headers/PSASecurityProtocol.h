// Copyright © Protectoria. All rights reserved.

NS_ASSUME_NONNULL_BEGIN

@protocol PSASecurityProtocol <NSObject>
@required
-(void) setDataHidden:(BOOL) isDataHidden;
@end

NS_ASSUME_NONNULL_END

