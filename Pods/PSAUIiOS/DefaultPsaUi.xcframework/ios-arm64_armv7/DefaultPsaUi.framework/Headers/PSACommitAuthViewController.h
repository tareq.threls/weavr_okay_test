// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import <PSACommon/PSACommon.h>
#import <DefaultPsaUi/DefaultPsaUi.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PSACommitAuthViewControllerDelegate;

@interface PSACommitAuthViewController : UIViewController

+ (instancetype)controllerWithTheme:(nullable PSATheme *)theme
                           delegate:(id<PSACommitAuthViewControllerDelegate>)delegate
                           authData:(PSAAuthData *)authData;

@end

@protocol PSACommitAuthViewControllerDelegate <NSObject>
- (void)commitAuthViewControllerCancelled:(PSACommitAuthViewController *)controller isTimeoutError:(BOOL)isTimeoutError;;
- (void)commitAuthViewControllerConfirmed:(PSACommitAuthViewController *)controller;
@end

NS_ASSUME_NONNULL_END
