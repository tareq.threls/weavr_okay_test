// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>
@interface PSAAuthTimer : NSObject

- (instancetype)initWithTime:(int) seconds
                    callback:(void(^)(NSString *, int, float)) callback;

- (void)appendCallback:(void(^)(NSString *, int, float)) callback;
- (void)removeLastCallback;
- (NSInteger)callbacksCount;
- (void)start;
- (void)invalidate;
@end

