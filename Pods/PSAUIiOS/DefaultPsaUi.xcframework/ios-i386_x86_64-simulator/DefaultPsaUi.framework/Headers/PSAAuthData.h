// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import "PSAPayment.h"

@interface PSAAuthData : NSObject

@property NSAttributedString *description;
@property NSArray<PSAPayment *> *payments;
@property NSInteger timerSeconds;
@property NSAttributedString *biometricAlertReasonText;
@property NSAttributedString *confirmActionHeaderText;
@property NSAttributedString *confirmButtonText;
@property NSAttributedString *confirmBiometricTouchButtonText;
@property NSAttributedString *confirmBiometricFaceButtonText;
@property NSAttributedString *cancelButtonText;
@property NSAttributedString *massPaymentDetailsButtonText;
@property NSAttributedString *massPaymentDetailsHeaderText;
@property NSAttributedString *feeLabelText;
@property NSAttributedString *recepientLabelText;
@property NSAttributedString *confirmationDescription;
@end
