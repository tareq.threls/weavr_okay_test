// Copyright © Protectoria. All rights reserved.

@interface PSAEnrollmentInitData : NSObject

@property NSAttributedString *bindingDeviceTitle;
@property NSAttributedString *bindingDeviceDescription;

- (instancetype)initWithTitle:(NSAttributedString *)title description:(NSAttributedString *)description;
@end
