// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import <PSACommon/PSACommon.h>
#import <DefaultPsaUi/DefaultPsaUi.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PSAPinViewControllerDelegate <NSObject>
- (void)authComplete:(NSString *)pin;
- (void)authCancelWithTimeoutError:(BOOL)timeoutError;
@end

@interface PSAPinViewController : UIViewController

+ (instancetype)controllerWithTheme:(nullable PSATheme *)theme
                           delegate:(id<PSAPinViewControllerDelegate>)delegate
                         isFallback:(bool)isFallback
                           authData:(PSAAuthData *)authData;

@end
NS_ASSUME_NONNULL_END
