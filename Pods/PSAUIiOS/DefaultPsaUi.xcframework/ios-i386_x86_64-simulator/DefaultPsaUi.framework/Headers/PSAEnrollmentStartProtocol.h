// Copyright © Protectoria. All rights reserved.

@protocol PSAEnrollmentStartProtocol

- (void)start;

@end
