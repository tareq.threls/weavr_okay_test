// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import "PSAAuthTimer.h"
#import "PSAAuthData.h"
NS_ASSUME_NONNULL_BEGIN

@interface PaymentDetailsViewController : UIViewController <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UITableView *paymentsTableView;
@property (weak, nonatomic) IBOutlet UILabel *massPaymentHeader;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (void)setupWithTimer:(PSAAuthTimer *)timer
              initData:(PSAAuthData *)initData
             timerText:(NSString *)timerText
         timerProgress:(float)timerProgress;

@end
NS_ASSUME_NONNULL_END
