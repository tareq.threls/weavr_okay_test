// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import <PSACommon/PSACommon.h>
#import <DefaultPsaUi/DefaultPsaUi.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PSABiometricAuthViewControllerDelegate <NSObject>
- (void)biometricAuthViewControllerCancelledWithTimeout:(BOOL)timeoutError;
- (void)biometricAuthViewControllerConfirmedWithType:(NSString *)biometricType;
- (void)fallbackToPin;
@end

@interface PSABiometricAuthViewController : UIViewController

+ (instancetype)controllerWithTheme:(nullable PSATheme *)theme
                           delegate:(id<PSABiometricAuthViewControllerDelegate>)delegate
                           authData:(PSAAuthData *)authData;

@end


NS_ASSUME_NONNULL_END
