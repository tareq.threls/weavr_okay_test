// Copyright © Protectoria. All rights reserved.

@protocol PSAForceClosable <NSObject>

- (void)forceClose;

@end
