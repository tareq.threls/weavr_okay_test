// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>

@interface PSAPayment : NSObject
@property double amount;
@property double fee;
@property NSString *recipient;
@property NSString *currency;

- (instancetype)initWithAmount:(double)amount
                           fee:(double)fee
                     recipient:(NSString *)recipient
                      currency:(NSString *)currency;

- (NSMutableAttributedString*)attributedAmountWithFontSize:(NSInteger)fontSize;

@end
