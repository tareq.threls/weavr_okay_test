// Copyright © Protectoria. All rights reserved.

#import <UIKit/UIKit.h>
#import "PSACommon/PSASharedStatuses.h"
#import "PSAEnrollmentStartProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface PSAEnrollmentViewController : UIViewController

@property (strong, nonatomic) id <PSAEnrollmentStartProtocol> enrollmentController;
@property (copy, nonatomic, nullable) void (^completion)(PSASharedStatuses);
@property (nullable) id enrollmentInitData;

- (void)startInvisibly;

@end

NS_ASSUME_NONNULL_END
