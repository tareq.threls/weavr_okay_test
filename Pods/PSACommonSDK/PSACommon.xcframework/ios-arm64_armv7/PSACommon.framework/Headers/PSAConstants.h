#import <Foundation/Foundation.h>

// Artifacts
extern NSString *const dictionaryKeyArtifacts;
extern NSString *const dictionaryKeyArtifactsValue;
extern NSString *const dictionaryKeyArtifactsClassName;
extern NSString *const dictionaryValueArtifactsSession;
extern NSString *const dictionaryValueArtifactsSimple;
extern NSString *const dictionaryKeyArtifactsApnsEnabled;
extern NSString *const dictionaryKeyArtifactsBiometryEnabled;
extern NSString *const dictionaryKeyRequestHeaderType;
extern NSString *const deviceInfoArtifactTypeString;

// RequestHeader
extern const long deviceInfoArtifactType;
