// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>

extern NSString *const PSAClientActionPathEnrollment;
extern NSInteger const PSAClientActionTypePrepareEnrollment;
extern NSInteger const PSAClientActionTypeCommitEnrollment;

extern NSString *const PSAClientActionPathAuth;
extern NSInteger const PSAClientActionTypePrepareAuth;
extern NSInteger const PSAClientActionTypeCommitAuth;
extern NSInteger const PSAClientActionTypeWakeUp;
extern NSInteger const PSAClientActionTypeHandshakeStep1;
extern NSInteger const PSAClientActionTypeHandshakeStep2;

extern NSString *const PSAClientActionPathLinkTenant;
extern NSInteger const PSAClientActionTypeLinkTenant;
extern NSInteger const PSAClientActionTypeUpdateTenant;
extern NSInteger const PSAClientActionTypeUnlinkTenant;

extern NSString *const PSAClientActionPathDeviceInfo;
extern NSInteger const PSAClientActionTypeDeviceInfo;
