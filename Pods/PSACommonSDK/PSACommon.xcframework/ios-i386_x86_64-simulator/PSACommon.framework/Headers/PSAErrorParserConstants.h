// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>

enum {
    Default_no_internet_status_code = -9,
    Default_runtime_error_code = 17,
    Default_server_has_active_session_code = -1,
    Default_server_error_code = 101
};
