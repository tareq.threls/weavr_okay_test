// Copyright © Protectoria. All rights reserved.

#import <Foundation/Foundation.h>

@interface PSACryptUtilsMD5 : NSObject
+ (nullable NSData *)encode:(nullable NSData *)data;
@end
